#include <iostream>
#include <fstream>
#include <string>
using namespace std;
class User {
	public:
	    string username;
	    string password;
};

void registerUser() {
    User user;
    cout << "Enter username: ";
    cin >> user.username;
    cout << "Enter password: ";
    cin >> user.password;

    ofstream file("users.bin", ios::binary | ios::app);
    file.write(reinterpret_cast<char*>(&user), sizeof(User));
    file.close();

    cout << "User registered successfully!" << endl;
}

bool loginUser() {
    User user;
    cout << "Enter username: ";
    cin >> user.username;
    cout << "Enter password: ";
    cin >> user.password;

    ifstream file("users.bin", ios::binary);
    if (!file) {
        cout << "No users found. Please register first." << endl;
        return false;
    }

    User storedUser;
    while (file.read(reinterpret_cast<char*>(&storedUser), sizeof(User))) {
        if (storedUser.username == user.username && storedUser.password == user.password) {
            file.close();
            cout << "Login successful!" << endl;
            return true;
        }
    }

    file.close();
    cout << "Invalid username or password. Please try again." << endl;
    return false;
}

void logoutUser() {
    cout << "Logout successful!" << endl;
}

int main() {
    int choice;
    bool isLoggedIn = false;

    do {
        cout << "1. Register\n2. Login\n3. Logout\n4. Exit\nEnter your choice: ";
        cin >> choice;

        switch (choice) {
            case 1:
                registerUser();
                break;
            case 2:
                if (!isLoggedIn) {
                    isLoggedIn = loginUser();
                } else {
                    cout << "Already logged in." << endl;
                }
                break;
            case 3:
                if (isLoggedIn) {
                    logoutUser();
                    isLoggedIn = false;
                } else {
                    cout << "You are not logged in." << endl;
                }
                break;
            case 4:
                cout << "Exiting..." << endl;
                break;
            default:
                cout << "Invalid choice. Please try again." << endl;
                break;
        }
    } while (choice != 4);

    return 0;
}

